const initialState = [];

function albums (state = initialState, action){
    switch(action.type) {
        case 'ALBUMS_LOADED':
            return [...action.payload];
        default:
            return state;
    }
}

export default albums