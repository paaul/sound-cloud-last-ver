package ua.danit.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.danit.model.Album;
import ua.danit.service.AlbumService;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;


@RestController
//@RequestMapping(path = "")
public class AlbumController {

    @Autowired
    private AlbumService albumService;

    //        3.1 GET /api/albums/{id}
    @RequestMapping(value = "/api/albums/{id}", method = RequestMethod.GET)
    public ResponseEntity<Album> getById(@PathVariable Long id) {
        return new ResponseEntity<>(albumService.getById(id), HttpStatus.OK);
    }

    // get all albums
    @RequestMapping(path = "/api/albums/all", method = RequestMethod.GET)
    public ResponseEntity<List<Album>> getAllAlbums() {
        return new ResponseEntity<>(albumService.getAllAlbums(), HttpStatus.OK);
    }

    //        3.2 PUT /api/albums/{id}
    @RequestMapping(value = "/api/albums/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Album> addAlbum(@RequestBody Album album, @PathVariable Long id) {
        album.setId(id);
        albumService.addAlbum(album);
        return new ResponseEntity<>(album, HttpStatus.OK);
    }

    //        3.3 DELETE /api/albums/{id}
    @RequestMapping(value = "/api/albums/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<HttpStatus> deleteAlbum(@PathVariable Long id) {
        albumService.deleteAlbum(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

//            3.4 DELETE /api/albums/{id}/cover
    @RequestMapping(path = "/api/albums/{id}/cover", method = RequestMethod.DELETE)
    public ResponseEntity<Album> deleteCover(@PathVariable Long id) {
        albumService.deleteCover(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


//
    //  for admin      PUT /api/albums/{id}
//     @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
//    public ResponseEntity<Album> addAlbum(@RequestBody Album album, @PathVariable Long id) {
//        album.setId(id);
//        albumService.addAlbum(album);
//        return new ResponseEntity<>(album, HttpStatus.OK);
//    }
//
//
//    //  for admin      DELETE /api/albums/{id}
//    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
//    public ResponseEntity<Album> deleteAlbumAdmin() {
//        return null;
//    }

}