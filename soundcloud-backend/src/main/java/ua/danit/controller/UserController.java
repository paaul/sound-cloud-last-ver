package ua.danit.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.danit.model.User;
import ua.danit.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/{email}/{name}", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getByEmailAndName(@PathVariable String email,
                                                         @PathVariable String name) {
        return new ResponseEntity<>(userService
                .findByEmailAndName(email, name), HttpStatus.OK);
    }

    @RequestMapping(value = "/{token}", method = RequestMethod.GET)
    public ResponseEntity<User> findByToken(@PathVariable String token) {
        return userService.findByToken(token)
                .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<User>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(path = "/", method = RequestMethod.POST)
    public User addUser(@RequestBody User user) {
        return userService.addUser(user);
    }

    @RequestMapping(path = "/get-all-users", method = RequestMethod.GET)
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

}
