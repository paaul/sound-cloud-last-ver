package ua.danit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.danit.model.Track;
import ua.danit.service.TrackService;

import java.util.List;

@RestController
@RequestMapping(path = "")
public class TrackController {

    @Autowired
    private TrackService trackService;

    // 3.1 GET /api/tracks
    @RequestMapping(value = "/api/tracks", method = RequestMethod.GET)
    @ResponseBody
       public ResponseEntity<List<Track>> getAllTracks() {
        return new ResponseEntity<>(trackService.getAllTracks(), HttpStatus.OK);
    }

    // 3.2 GET /api/tracks/{id}
    @RequestMapping(value = "/api/tracks/{id}", method = RequestMethod.GET)
    public ResponseEntity<Track> getById(@PathVariable Long id) {
        return new ResponseEntity<>(trackService.getById(id), HttpStatus.OK);
    }

    // 3.3 POST /api/tracks
    @RequestMapping(path = "/api/tracks", method = RequestMethod.POST)
    public ResponseEntity<Track> addTrack(@RequestBody Track track) {
        trackService.addTrack(track);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    //        3.4 PUT /api/tracks/{id}
    @RequestMapping(value = "/api/tracks/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Track> addTrack(@RequestBody Track track, @PathVariable Long id) {
        track.setId(id);
        trackService.addTrack(track);
        return new ResponseEntity<>(track, HttpStatus.OK);
    }

    //        3.5 DELETE /api/tracks/{id}
    @RequestMapping(value = "/api/tracks/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<HttpStatus> deleteTrack(@PathVariable Long id){
        trackService.deleteTrack(id);
        return  new ResponseEntity<>(HttpStatus.OK);
    }

    //        3.6 POST /api/tracks/{id}/like
    @RequestMapping(value = "/api/tracks/{id}/like", method = RequestMethod.POST)
    public ResponseEntity<Track> postLike(@RequestBody Track track) {
        trackService.postLike(track);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    //        3.7 GET /api/tracks?liked=true
    @RequestMapping(value = "/api/tracks?liked=true", method = RequestMethod.GET)
    public ResponseEntity<List<Track>> getLikedTracks(@PathVariable Long id) {
        return new ResponseEntity<>(trackService.getLikedTracks(id), HttpStatus.OK);
    }

    //        3.8 POST /api/tracks/{id}/song mp3 upload.
    @RequestMapping(value = "/api/tracks/{id}/song", method = RequestMethod.POST)
    public ResponseEntity<Track> uploadTrack(@PathVariable Long id) {
        return new ResponseEntity<>();
    }

}

