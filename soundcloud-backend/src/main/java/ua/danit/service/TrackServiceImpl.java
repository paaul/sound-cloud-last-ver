package ua.danit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.dao.TrackDao;
import ua.danit.dao.UserDao;
import ua.danit.model.Track;

import java.util.List;

@Service
public class TrackServiceImpl implements TrackService {

    @Autowired
    private TrackDao trackDao;
    private UserDao userDao;


    @Override
    public List<Track> getAllTracks() {
        return (List<Track>) trackDao.findAll(); // class cast iterable list
    }

    @Override
    public void addTrack(Track track) {

    }

    @Override
    public Track getById(Long id) {
        return trackDao.findOne(id);
    }

    @Override
    public void deleteTrack(Long id) {
        trackDao.delete(id);
    }

    @Override
    public postLike(Long id) {
        trackDao.findOne(id);
        userDao.findOne(id);
//        user - track
//        track - user



//        return ;
    }

    @Override
    public List<Track> getLikedTracks(Long id) {
        return (List<Track>) trackDao.findAllByUser_id(id);
    }
}


