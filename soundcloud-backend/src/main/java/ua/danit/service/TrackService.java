package ua.danit.service;


import ua.danit.model.Track;

import java.util.List;

public interface TrackService {

    List<Track> getAllTracks();

    void  addTrack(Track track);

    Track getById(Long id);

    void deleteTrack(Long id);

    Track postLike(Track track);

    List<Track> getLikedTracks(Long id);
}
