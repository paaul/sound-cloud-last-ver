package ua.danit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import ua.danit.dao.AlbumDao;
import ua.danit.model.Album;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;


@Service
public class AlbumServiceImpl implements AlbumService {

    @Autowired
    private AlbumDao albumDao;

    @Value("${files.path.suffix}")
    private String env;


    @Override
    public List<Album> getAllAlbums() {
        return albumDao.findAll();
    }

    @Override
    public void addAlbum(Album album) {

    }

    @Override
    public Album getById(Long id) {
        return albumDao.findOne(id);
    }

    @Override
    public void deleteAlbum(Long id) {
//      Album album = albumDao.findOne(id);
//      album.setAlbumCover(null);
//      album.setId(null);
//      album.setAlbumTitle(null);
//      albumDao.save(album);

        albumDao.delete(id);

        //проверить на каскадное удаление треков
    }

    @Override
    public void deleteCover(Long id) {
        Album album = albumDao.findOne(id);
//        System.out.println(env);
        album.setAlbumCover(null);
        albumDao.save(album);
    }
}
