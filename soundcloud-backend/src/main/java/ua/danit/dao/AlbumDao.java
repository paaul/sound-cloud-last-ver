package ua.danit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.danit.model.Album;

public interface AlbumDao extends JpaRepository<Album, Long> {

}





