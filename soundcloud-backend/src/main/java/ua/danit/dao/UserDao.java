package ua.danit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import ua.danit.model.User;

import java.util.List;

public interface UserDao extends JpaRepository<User, Long> {

    //это вообще нужно?
    List<User> findByEmailAndName(String email, String name);

}
