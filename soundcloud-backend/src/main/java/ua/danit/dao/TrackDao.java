package ua.danit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import ua.danit.model.Track;

import java.util.List;


public interface TrackDao extends JpaRepository<Track, Long> {

    List<Track>findAllByUser_id(Long id);

}
