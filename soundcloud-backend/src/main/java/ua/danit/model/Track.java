package ua.danit.model;

import com.google.common.base.MoreObjects;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "TRACKS")
public class Track {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "TRACK_TITLE")
    private String trackName;

    @Column(name = "ALBUM_TITLE")
    private String albumName;

    //MANY TO MANY start

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "likes",
            joinColumns = { @JoinColumn(name = "track_id")},
            inverseJoinColumns = { @JoinColumn(name = "users_id") }
    )
    Set<User> users = new HashSet<>();

    //MANY TO MANY end

    public Track(String trackName) {
        this.trackName = trackName;
    }

    public Track() {

    }

    public Long getById() {
        return id;
    }

    public String getTrackName() {
        return trackName;
    }

    public String getAlbumName() {
        return albumName;
    }

    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("track_id", id)
                .add("trackName", trackName)
                .add("albumName", albumName)
                .toString();
    }

    public Track(String trackName, String albumName) {
        this.trackName = trackName;
        this.albumName = albumName;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
