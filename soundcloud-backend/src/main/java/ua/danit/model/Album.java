package ua.danit.model;

import javax.persistence.*;
import java.io.File;

@Entity
@Table(name = "ALBUMS")
public class Album {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "ALBUM_TITLE")
    private String albumTitle;
    @Column(name = "ALBUM_COVER")
    private String albumCover;



    public String getAlbumCover() {
        return albumCover;
    }

    public void setAlbumCover(String albumCover) {
        this.albumCover = albumCover;
    }

    public Album(String albumTitle, String albumCover) {
        this.albumCover = albumCover;
        this.albumTitle = albumTitle;
    }

    public Album() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAlbumTitle() {
        return albumTitle;
    }

    public void setAlbumTitle(String albumTitle) {
        this.albumTitle = albumTitle;
    }
}